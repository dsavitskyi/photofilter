package com.app.multimedia

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.photo_item.view.*

class PhotoAdapter(
    val activity: HomeActivity,
    val photos: List<Photo>,
    val preview: ImageView,
    val bottomSheetBehavior: BottomSheetBehavior<View>
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var selectedPos = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.photo_item, parent, false)
        return PhotoViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val photo = photos.get(holder.adapterPosition)
        val viewHolder = holder as PhotoViewHolder

        Glide.with(viewHolder.photoView.context)
            .load(Uri.parse(photo.uri))
            .placeholder(R.drawable.default_background)
            .into(viewHolder.photoView)

        viewHolder.photoView.setOnClickListener {
            selectedPos = holder.adapterPosition
            notifyDataSetChanged()

            Glide.with(viewHolder.photoView.context)
                .asBitmap()
                .load(Uri.parse(photo.uri))
                .into(object : CustomTarget<Bitmap>(){
                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: Transition<in Bitmap>?
                    ) {
                        activity.photoView.setImageBitmap(resource)
                        preview.setImageBitmap(resource)
                        activity.originalBitmapDrawable = BitmapDrawable(activity.resources, resource)
                        activity.updateFiltersBtn(resource)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {}
                })
            preview.visibility = View.VISIBLE
        }
        if(selectedPos == holder.adapterPosition) {
            viewHolder.photoView.alpha = 0.25f
        }else {
            viewHolder.photoView.alpha = 1.0f
        }
    }

    override fun getItemCount(): Int {
        return photos.size
    }

    class PhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val photoView = itemView.photoView
    }
}