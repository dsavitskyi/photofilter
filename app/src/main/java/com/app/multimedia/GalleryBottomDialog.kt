package com.app.multimedia

import android.app.Dialog
import android.content.res.Resources
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.gallery.*

class GalleryBottomDialog(val photos: List<Photo>) : BottomSheetDialogFragment() {

    private lateinit var dialog: BottomSheetDialog
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>
    private lateinit var rootView: View

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        rootView = inflater.inflate(R.layout.gallery, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bottomSheetBehavior = BottomSheetBehavior.from(view.parent as View)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED

        galleryContainer.minimumHeight = Resources.getSystem().displayMetrics.heightPixels

        ivApply.setOnClickListener {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
        ivClose.setOnClickListener {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
        //preview photo
        Glide.with(requireActivity())
            .load(Uri.parse(photos.get(0).uri))
            .placeholder(R.drawable.default_background)
            .into(preview)

        val layoutManager = GridLayoutManager(context, 4)
        rvPhotos.layoutManager = layoutManager
        val adapter = PhotoAdapter(activity as HomeActivity, photos, preview, bottomSheetBehavior)
        rvPhotos.adapter = adapter
        rvPhotos.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!rvPhotos.canScrollVertically(-1)) {
                    preview.visibility = View.VISIBLE
                } else {
                    preview.visibility = View.GONE
                }
            }
        })
    }
}