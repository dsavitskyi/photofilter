package com.app.multimedia

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.*
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import com.app.multimedia.MainActivity.Companion.PHOTO_CROPPED
import com.app.multimedia.databinding.ActivityHomeBinding
import java.io.FileNotFoundException
import java.io.OutputStream
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HomeActivity : AppCompatActivity() {

    private lateinit var viewBinding: ActivityHomeBinding
    lateinit var originalBitmapDrawable: BitmapDrawable
    private var filteredBmp: Bitmap? = null
    private var filtered: String? = null
    private var storagePermissionsLauncher: ActivityResultLauncher<String>? = null
    private val storagePermission = WRITE_EXTERNAL_STORAGE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        originalBitmapDrawable = viewBinding.photoView.drawable as BitmapDrawable
        initViews()
        storagePermissionsLauncher = registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if(isGranted){
                getPhotos()
            } else {
                respondToUserOnPermissionAction()
            }
        }
    }

    private val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val intent = result.data
            intent?.getParcelableExtra<Uri>(PHOTO_CROPPED)?.let {
                setBitmapFromUri(it)
            }
        }
    }

    private fun respondToUserOnPermissionAction() {
        if (ContextCompat.checkSelfPermission(this, storagePermission) == PackageManager.PERMISSION_GRANTED) {
            getPhotos()
        } else {
            if (shouldShowRequestPermissionRationale(storagePermission)) {
                AlertDialog.Builder(this)
                    .setTitle("Requesting permission")
                    .setMessage("Allow to show and save photos")
                    .setPositiveButton("Allow", object : DialogInterface.OnClickListener{
                        override fun onClick(p0: DialogInterface?, p1: Int) {
                            //request permission
                            storagePermissionsLauncher?.launch(storagePermission)
                        }
                    })
                    .setNegativeButton("Cancel", object : DialogInterface.OnClickListener{
                        override fun onClick(p0: DialogInterface?, p1: Int) {
                            p0?.dismiss()
                        }
                    })
                    .show()
            }
        }
    }

    private fun getPhotos() {
        val photos: ArrayList<Photo> = ArrayList()
        val libraryUri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL)
        } else {
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        }
        val projection = arrayOf(
            MediaStore.Images.Media._ID,
            MediaStore.Images.Media.DISPLAY_NAME,
            MediaStore.Images.Media.SIZE,
            MediaStore.Images.Media.DATE_ADDED,
            MediaStore.Images.Media.BUCKET_ID,
            MediaStore.Images.Media.BUCKET_DISPLAY_NAME
        )
        //sort order
        val sortOrder = MediaStore.Images.Media.DATE_ADDED.plus(" DESC")
        try {
            val cursor: Cursor? = contentResolver.query(libraryUri, projection, null, null, sortOrder)
            val idColumn = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
            val nameColumn = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)
            val sizeColumn = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.SIZE)
            val dateColumn = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.DATE_ADDED)
            val bucketIdColumn = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_ID)
            val bucketNameColumn = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME)

            while (cursor?.moveToNext() == true) {
                val id = cursor.getLong(idColumn?: 0)
                var name = cursor.getString(nameColumn?: 0)
                val date = cursor.getString(dateColumn?: 0)
                val size = cursor.getInt(sizeColumn?: 0)
                val bucketId = cursor.getLong(bucketIdColumn?: 0)
                val bucketName = cursor.getString(bucketNameColumn?: 0)

                //photo uri
                val uri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id)
                //remove .png extension on photo name
                //name = name.substring(0, name.lastIndexOf("."))

                //photoItem
                val photo = Photo(id, uri.toString(), name, date, size, bucketId, bucketName)
                photos.add(photo)
            }
            cursor?.close()
            showPhotos(photos)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showPhotos(photos: List<Photo>) {
        if (photos.isNotEmpty()) {
            Toast.makeText(this, "Photos available: ${photos.size}", Toast.LENGTH_SHORT).show()
            val galleyDialog = GalleryBottomDialog(photos)
            galleyDialog.show(supportFragmentManager, galleyDialog.tag)
        } else {
            Toast.makeText(this, "No Available photos", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setBitmapFromUri(uri: Uri) {
        val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
        viewBinding.photoView.setImageBitmap(bitmap)
        originalBitmapDrawable = viewBinding.photoView.drawable as BitmapDrawable
        updateFiltersBtn(bitmap)
    }

    fun updateFiltersBtn(bitmap: Bitmap) {
        with(viewBinding.filterLayout) {
            filterOgBtn.setImageBitmap(bitmap)
            greyBtn.setImageBitmap(bitmap)
            redBtn.setImageBitmap(bitmap)
            greenBtn.setImageBitmap(bitmap)
            blueBtn.setImageBitmap(bitmap)
            redGreenBtn.setImageBitmap(bitmap)
            redBlueBtn.setImageBitmap(bitmap)
            greenBlueBtn.setImageBitmap(bitmap)
            sepiaBtn.setImageBitmap(bitmap)
            binaryBtn.setImageBitmap(bitmap)
            invertBtn.setImageBitmap(bitmap)
        }
        filters()
    }

    private fun initViews() {
        viewBinding.ivSaveBtn.setOnClickListener {
            AlertDialog.Builder(this)
                .setTitle("Are you sure?")
                .setMessage("Want to save photo to the gallery")
                .setPositiveButton("Save", object : DialogInterface.OnClickListener{
                    override fun onClick(p0: DialogInterface?, p1: Int) {
                        //have any edited bitmap
                        if (filteredBmp != null) {
                            savePhoto(filteredBmp)
                        }
                    }
                })
                .setNegativeButton("Cancel", object : DialogInterface.OnClickListener{
                    override fun onClick(p0: DialogInterface?, p1: Int) {
                        p0?.dismiss()
                    }
                })
                .show()
        }
        viewBinding.ivGalleryBtn.setOnClickListener {
            storagePermissionsLauncher?.launch(storagePermission)
        }
        viewBinding.ivCameraBtn.setOnClickListener {
            startForResult.launch(Intent(this, MainActivity::class.java))
        }
        viewBinding.filterBtn.setOnClickListener {
            viewBinding.toolsLayout.visibility = View.GONE
            viewBinding.filterLayout.filterBtnsLayout.visibility = View.VISIBLE
        }
        viewBinding.filterLayout.filterBackBtn.setOnClickListener {
            viewBinding.toolsLayout.visibility = View.VISIBLE
            viewBinding.filterLayout.filterBtnsLayout.visibility = View.GONE
        }
        viewBinding.brightnessBtn.setOnClickListener {
            viewBinding.toolsLayout.visibility = View.GONE
            viewBinding.brightnessLayout.brightnessSeekBarLayout.visibility = View.VISIBLE
        }
        viewBinding.brightnessLayout.brightnessSeekBarOkView.setOnClickListener {
            viewBinding.toolsLayout.visibility = View.VISIBLE
            viewBinding.brightnessLayout.brightnessSeekBarLayout.visibility = View.GONE
        }
        viewBinding.contrastBtn.setOnClickListener {
            viewBinding.contrastLayout.contrastSeekBarLayout.visibility = View.VISIBLE
            viewBinding.toolsLayout.visibility = View.GONE
        }
        viewBinding.contrastLayout.contrastSeekBarOkView.setOnClickListener {
            viewBinding.contrastLayout.contrastSeekBarLayout.visibility = View.GONE
            viewBinding.toolsLayout.visibility = View.VISIBLE
        }
        viewBinding.collageBtn.setOnClickListener {
            Toast.makeText(this, "Coming soon", Toast.LENGTH_SHORT).show()
        }
        viewBinding.textBtn.setOnClickListener {
            Toast.makeText(this, "Coming soon", Toast.LENGTH_SHORT).show()
        }
        viewBinding.shapeBtn.setOnClickListener {
            Toast.makeText(this, "Coming soon", Toast.LENGTH_SHORT).show()
        }
        filters()
        seekbarListeners()
    }

    @SuppressLint("SimpleDateFormat")
    private fun savePhoto(bitmap: Bitmap?) {
        if (ContextCompat.checkSelfPermission(this, storagePermission) == PackageManager.PERMISSION_GRANTED) {
            val contentResolver: ContentResolver = contentResolver
            if (isExternalStorageWritable()) {
                //photo store Uri
                val photoCollectionUri: Uri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
                } else {
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                }
                //photo name
                val timestamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
                val photoName = resources.getString(R.string.app_name).plus("_").plus(timestamp).plus(".jpg")
                //photo content
                val contentValues = ContentValues()
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, photoName)
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, "DCIM/".plus(resources.getString(R.string.app_name)))
                }
                //save the photo url
                val photoUri: Uri? = contentResolver.insert(photoCollectionUri, contentValues)
                //output stream t save the photo to the gallery
                try {
                    val fos: OutputStream? = contentResolver.openOutputStream(photoUri!!)
                    val isSaved = bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, fos)?: false
                    if (isSaved) {
                        Toast.makeText(this, "Photo saved to gallery", Toast.LENGTH_SHORT).show()
                    }
                    //close the fos
                    fos?.flush()
                    fos?.close()

                }catch (e: FileNotFoundException) {
                    e.printStackTrace()
                }
            }
        } else {
            storagePermissionsLauncher?.launch(storagePermission)
        }
    }

    private fun isExternalStorageWritable(): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }

    private fun filters() {
        //grey btn
        filterBtn(viewBinding.filterLayout.greyBtn, Filter.grey)
        viewBinding.filterLayout.greyBtn.setOnClickListener {
            //filter to grey scale
            filter(Filter.grey)
        }
        //reset filter btn
        viewBinding.filterLayout.filterOgBtn.setOnClickListener {
            resetFilter()
        }
        //red btn
        filterBtn(viewBinding.filterLayout.redBtn, Filter.red)
        viewBinding.filterLayout.redBtn.setOnClickListener {
            //filter to red scale
            filter(Filter.red)
        }
        //green btn
        filterBtn(viewBinding.filterLayout.greenBtn, Filter.green)
        viewBinding.filterLayout.greenBtn.setOnClickListener {
            //filter to red scale
            filter(Filter.green)
        }
        //blue btn
        filterBtn(viewBinding.filterLayout.blueBtn, Filter.blue)
        viewBinding.filterLayout.blueBtn.setOnClickListener {
            //filter to blue scale
            filter(Filter.blue)
        }
        //red green btn
        filterBtn(viewBinding.filterLayout.redGreenBtn, Filter.redGreen)
        viewBinding.filterLayout.redGreenBtn.setOnClickListener {
            //filter to red green scale
            filter(Filter.redGreen)
        }
        //red blue btn
        filterBtn(viewBinding.filterLayout.redBlueBtn, Filter.redBlue)
        viewBinding.filterLayout.redBlueBtn.setOnClickListener {
            //filter to red blue scale
            filter(Filter.redBlue)
        }
        //green blue btn
        filterBtn(viewBinding.filterLayout.greenBlueBtn, Filter.greenBlue)
        viewBinding.filterLayout.greenBlueBtn.setOnClickListener {
            //filter to red blue scale
            filter(Filter.greenBlue)
        }
        //sepia btn
        filterBtn(viewBinding.filterLayout.sepiaBtn, Filter.sepia)
        viewBinding.filterLayout.sepiaBtn.setOnClickListener {
            //filter to sepia scale
            filter(Filter.sepia)
        }
        //binary btn
        filterBtn(viewBinding.filterLayout.binaryBtn, Filter.binary)
        viewBinding.filterLayout.binaryBtn.setOnClickListener {
            //filter to binary scale
            filter(Filter.binary)
        }
        //invert btn
        filterBtn(viewBinding.filterLayout.invertBtn, Filter.invert)
        viewBinding.filterLayout.invertBtn.setOnClickListener {
            //filter to invert scale
            filter(Filter.invert)
        }
    }

    private fun resetFilter() {
        //clear filter
        filteredBmp = null
        filtered = null
        //set original photo to imageview
        viewBinding.photoView.setImageDrawable(originalBitmapDrawable)

        //reset seekbars
        viewBinding.brightnessLayout.brightnessSeekBar.progress = 0
        viewBinding.contrastLayout.contrastSeekBar.progress = 255 //set as for original photo
    }

    private fun filter(filter: String){
        //create bitmap from original bitmapDrawable
        val btm = originalBitmapDrawable.bitmap
        val outputBitmap = Bitmap.createScaledBitmap(btm!!, btm.width, btm.height, false).copy(Bitmap.Config.ARGB_8888, true)
        //define a paint for styling and coloring bitmaps
        val paint = Paint()
        //canvas to draw bitmap
        val canvas = Canvas(outputBitmap)

        //filtering the photo to greyscale
        if(filter == Filter.grey) {
            //colormatrix to filter to greyscale
            val colorMatrix = ColorMatrix()
            colorMatrix.setSaturation(0f)
            val colorMatrixColorFilter = ColorMatrixColorFilter(colorMatrix)
            paint.colorFilter = colorMatrixColorFilter

            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }

        //filtering the photo to redscale
        if (filter == Filter.red) {
            val mul = 0XFF0000 //max red, other than min 0
            val add = 0X000000 //constant at 0
            val colorFilter = LightingColorFilter(mul, add)
            paint.colorFilter = colorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }

        //filtering the photo to green filter
        if (filter == Filter.green) {
            val mul = 0X00FF00 //max green, other than min 0
            val add = 0X000000 //constant at 0
            val colorFilter = LightingColorFilter(mul, add)
            paint.colorFilter = colorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }

        //filtering the photo to blue filter
        if (filter == Filter.blue) {
            val mul = 0X0000FF //max blue, other than min 0
            val add = 0X000000 //constant at 0
            val colorFilter = LightingColorFilter(mul, add)
            paint.colorFilter = colorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }

        //filtering the photo to red green filter
        if (filter == Filter.redGreen) {
            val mul = 0XFFFF00 //max red and green but blue min 0
            val add = 0X000000 //constant at 0
            val colorFilter = LightingColorFilter(mul, add)
            paint.colorFilter = colorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }

        //filtering the photo to red blue filter
        if (filter == Filter.redBlue) {
            val mul = 0XFF00FF //max red and blue but min green
            val add = 0X000000 //constant at 0
            val colorFilter = LightingColorFilter(mul, add)
            paint.colorFilter = colorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }

        //filtering the photo to green blue filter
        if (filter == Filter.greenBlue) {
            val mul = 0X00FFFF //max green and blue but min red 0
            val add = 0X000000 //constant at 0
            val colorFilter = LightingColorFilter(mul, add)
            paint.colorFilter = colorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }

        //filtering the photo to sepia filter
        if (filter == Filter.sepia) {
            //use color matrix
            val colorMatrix = ColorMatrix()
            colorMatrix.setSaturation(0f)

            //color scale
            val colorScale = ColorMatrix()
            colorScale.setScale(1f, 1f, 0.8f, 1f)
            //convert color matrix to grey scale and then apply to brown color
            colorMatrix.postConcat(colorScale)
            //color matrix filter
            val colorMatrixColorFilter = ColorMatrixColorFilter(colorMatrix)
            paint.colorFilter = colorMatrixColorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }

        //filtering the photo to binary filter
        if (filter == Filter.binary) {
            //use color matrix
            val colorMatrix = ColorMatrix()
            colorMatrix.setSaturation(0f)
            //binary threshold
            val m = 255f
            val t = -255*128f
            val threshold = ColorMatrix(floatArrayOf(
                m, 0f, 0f, 1f, t,
                0f, m, 0f, 1f, t,
                0f, 0f, m, 1f, t,
                0f, 0f, 0f, 1f, 0f
            ))
            //convert color matrix to grey scale
            colorMatrix.postConcat(threshold)
            //color filter
            val colorMatrixColorFilter = ColorMatrixColorFilter(colorMatrix)
            paint.colorFilter = colorMatrixColorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }

        //filtering the photo to invert filter
        if (filter == Filter.invert) {
            //use color matrix
            val colorMatrix = ColorMatrix()
            colorMatrix.setSaturation(0f)
            colorMatrix.set(floatArrayOf(
                -1f, 0f, 0f, 1f, 255f,
                0f, -1f, 0f, 0f, 255f,
                0f, 0f, -1f, 0f, 255f,
                0f, 0f, 0f, 1f, 0f
            ))
            //color filter
            val colorMatrixColorFilter = ColorMatrixColorFilter(colorMatrix)
            paint.colorFilter = colorMatrixColorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }

        //set output bitmap to imageview
        viewBinding.photoView.setImageBitmap(outputBitmap)

        //save filtered resources
        filteredBmp = outputBitmap
        filtered = filter
    }

    private fun filterBtn(btn: ImageView, filter: String){
        //get bitmap drawable from imageview btn
        val bitmapDrawable = btn.drawable as BitmapDrawable

        //get bitmap from above drawable bitmap
        val btm = bitmapDrawable.bitmap
        val outputBitmap = Bitmap.createScaledBitmap(btm, btm.width, btm.height, false).copy(Bitmap.Config.ARGB_8888, true)
        //define a paint for styling and coloring bitmaps
        val paint = Paint()
        //canvas to draw bitmap
        val canvas = Canvas(outputBitmap)
        if (filter == Filter.grey) {
            //colormatrix to filter to greyscale
            val colorMatrix = ColorMatrix()
            colorMatrix.setSaturation(0f)
            val colorMatrixColorFilter = ColorMatrixColorFilter(colorMatrix)
            paint.colorFilter = colorMatrixColorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }
        //filtering red btn image to red filter
        if (filter == Filter.red) {
            val mul = 0XFF0000 //max red, other than min 0
            val add = 0X000000 //constant at 0
            val colorFilter = LightingColorFilter(mul, add)
            paint.colorFilter = colorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }
        //filtering green btn image to green filter
        if (filter == Filter.green) {
            val mul = 0X00FF00 //max green, other than min 0
            val add = 0X000000 //constant at 0
            val colorFilter = LightingColorFilter(mul, add)
            paint.colorFilter = colorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }
        //filtering green btn image to green filter
        if (filter == Filter.blue) {
            val mul = 0X0000FF //max blue, other than min 0
            val add = 0X000000 //constant at 0
            val colorFilter = LightingColorFilter(mul, add)
            paint.colorFilter = colorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }
        //filtering green btn to red green filter
        if (filter == Filter.redGreen) {
            val mul = 0XFFFF00 //max red and green but blue min 0
            val add = 0X000000 //constant at 0
            val colorFilter = LightingColorFilter(mul, add)
            paint.colorFilter = colorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }
        //filtering red blue btn to red blue filter
        if (filter == Filter.redBlue) {
            val mul = 0XFF00FF //max red and blue but min green
            val add = 0X000000 //constant at 0
            val colorFilter = LightingColorFilter(mul, add)
            paint.colorFilter = colorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }
        //filtering green blue btn to green blue filter
        if (filter == Filter.greenBlue) {
            val mul = 0X00FFFF //max green and blue but min red 0
            val add = 0X000000 //constant at 0
            val colorFilter = LightingColorFilter(mul, add)
            paint.colorFilter = colorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }
        //filtering sepia btn image to sepia filter
        if (filter == Filter.sepia) {
            //use color matrix
            val colorMatrix = ColorMatrix()
            colorMatrix.setSaturation(0f)

            //color scale
            val colorScale = ColorMatrix()
            colorScale.setScale(1f, 1f, 0.8f, 1f)
            //convert color matrix to grey scale and then apply to brown color
            colorMatrix.postConcat(colorScale)
            //color matrix filter
            val colorMatrixColorFilter = ColorMatrixColorFilter(colorMatrix)
            paint.colorFilter = colorMatrixColorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }
        //filtering the binary btn to binary filter
        if (filter == Filter.binary) {
            //use color matrix
            val colorMatrix = ColorMatrix()
            colorMatrix.setSaturation(0f)
            //binary threshold
            val m = 255f
            val t = -255*128f
            val threshold = ColorMatrix(floatArrayOf(
                m, 0f, 0f, 1f, t,
                0f, m, 0f, 1f, t,
                0f, 0f, m, 1f, t,
                0f, 0f, 0f, 1f, 0f
            ))
            //convert color matrix to grey scale
            colorMatrix.postConcat(threshold)
            //color filter
            val colorMatrixColorFilter = ColorMatrixColorFilter(colorMatrix)
            paint.colorFilter = colorMatrixColorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }
        //filtering the invert btn image to invert filter
        if (filter == Filter.invert) {
            //use color matrix
            val colorMatrix = ColorMatrix()
            colorMatrix.setSaturation(0f)
            colorMatrix.set(floatArrayOf(
                -1f, 0f, 0f, 1f, 255f,
                0f, -1f, 0f, 0f, 255f,
                0f, 0f, -1f, 0f, 255f,
                0f, 0f, 0f, 1f, 0f
            ))
            //color filter
            val colorMatrixColorFilter = ColorMatrixColorFilter(colorMatrix)
            paint.colorFilter = colorMatrixColorFilter
            //draw our bitmap
            canvas.drawBitmap(outputBitmap, 0f,0f, paint)
        }
        //set output bitmap to imageview btn
        btn.setImageBitmap(outputBitmap)
    }

    private fun seekbarListeners() {
        viewBinding.brightnessLayout.brightnessSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(p0: SeekBar?, progress: Int, p2: Boolean) {
                adjustBrightness(progress)
            }
            override fun onStartTrackingTouch(p0: SeekBar?) {}

            override fun onStopTrackingTouch(p0: SeekBar?) {}
        })
        viewBinding.contrastLayout.contrastSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(p0: SeekBar?, progress: Int, p2: Boolean) {
                adjustContrast(progress)
            }
            override fun onStartTrackingTouch(p0: SeekBar?) {}

            override fun onStopTrackingTouch(p0: SeekBar?) {}
        })
    }

    private fun adjustBrightness(value: Int) {
        //bitmap from original bitmap drawable
        var bmp = originalBitmapDrawable.bitmap
        if (filteredBmp != null) {
            bmp = filteredBmp
        }
        //define a mul
        val mul = 0XFFFFFF //must  be a constant at 255
        //define an add
        val initialHex = Tools.hexScale()[value]
        val initialAdd = "0X$initialHex$initialHex$initialHex" //0XF4F4F4
        val add = Integer.decode(initialAdd)
        //generate bitmap from above bitmap
        val outputBitmap = Bitmap.createScaledBitmap(bmp, bmp.width, bmp.height, false).copy(Bitmap.Config.ARGB_8888, true)
        val paint = Paint()
        val colorFilter = LightingColorFilter(mul, add)
        paint.colorFilter = colorFilter
        val canvas = Canvas(outputBitmap)
        canvas.drawBitmap(outputBitmap, 0f, 0f, paint)
        //set out bitmap to imageview
        viewBinding.photoView.setImageBitmap(outputBitmap)
    }

    private fun adjustContrast(value: Int) {
        //bitmap from original bitmap drawable
        var bmp = originalBitmapDrawable.bitmap
        if (filteredBmp != null) {
            bmp = filteredBmp
        }
        //define a mul
        val add = 0X000000
        //define an add
        val initialHex = Tools.hexScale()[value]
        val initialMul = "0X$initialHex$initialHex$initialHex" //0X8D8D8D
        val mul = Integer.decode(initialMul)
        //generate bitmap from above bitmap
        val outputBitmap = Bitmap.createScaledBitmap(bmp, bmp.width, bmp.height, false).copy(Bitmap.Config.ARGB_8888, true)
        val paint = Paint()
        val colorFilter = LightingColorFilter(mul, add)
        paint.colorFilter = colorFilter
        val canvas = Canvas(outputBitmap)
        canvas.drawBitmap(outputBitmap, 0f, 0f, paint)
        //set out bitmap to imageview
        viewBinding.photoView.setImageBitmap(outputBitmap)
    }
}