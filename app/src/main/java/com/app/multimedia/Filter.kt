package com.app.multimedia

class Filter {
    companion object{
        const val grey = "grey"
        const val red = "red"
        const val green = "green"
        const val blue = "blue"
        const val redGreen = "redGreen"
        const val redBlue = "redBlue"
        const val greenBlue = "greenBlue"
        const val sepia = "sepia"
        const val binary = "binary"
        const val invert = "invert"
    }
}