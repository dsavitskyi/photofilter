package com.app.multimedia

data class Photo(
    val id: Long,
    val uri: String,
    val name: String,
    val date: String,
    val size: Int,
    val bucketId: Long,
    val bucketName: String
)
